import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static person getInputFromUser(){

        System.out.print("Enter name: ");
        String name = scanner.next();
        System.out.print("Enter address: ");
        String address = scanner.next();

        return new person(name ,address);
    }
    static void insertIntoDB(Connection connection){
        String insert="INSERT INTO pvh_tb VALUES (default,?,?)";
        person p=getInputFromUser();
        try{
            PreparedStatement preparedStatement=connection.prepareStatement(insert);
            preparedStatement.setString(1,p.getName());
            preparedStatement.setString(2,p.getAddress());
            int row=preparedStatement.executeUpdate();
            if(row>0){
                System.out.printf("\n=> Insert Success..!!!");
            }else{
                System.out.printf("Failed...!!");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

    }
    static void updateDB(Connection connection){
        String updateQuery="UPDATE pvh_tb SET name=? ,address=? where id=?";
        int id;
        System.out.print("Input ID to Update: "); id=scanner.nextInt();
        person p=getInputFromUser();
       try {
           PreparedStatement preparedStatement=connection.prepareStatement(updateQuery);
           preparedStatement.setString(1,p.getName());
           preparedStatement.setString(2,p.getAddress());
           preparedStatement.setInt(3,id);
          int row= preparedStatement.executeUpdate();
           if(row>0){
               System.out.printf("\n=> Update Success..!!!");
           }else{
               System.out.printf("Failed...!!");
           }
       }catch (SQLException e){
           e.printStackTrace();
       }

    }

    public static void main(String[] args) {
        ConnectionDB connectionDB=new ConnectionDB();
        Connection connection=connectionDB.getconConnection();

        insertIntoDB(connection);
        updateDB(connection);


    }
}
